# 中华人民共和国省市区街四级联动地址选择

#### 项目介绍
省市区街四级联动地址选择

#### 软件架构
包含三个目录，分别用于PC，微信网页版，微信小程序

PC版中的location.html为面向过程方式，locationOOP为面向对象方式，locationOOP-ES6为ES6中类的写法。

微信网页版中跟上面相同。


#### 安装教程

无

#### 使用说明

微信小程序使用：

1. 将目录名换成合适的英文名，放到小程序目录中

2.  在需要使用的主页面中引用，比如主页面名为test

3.  在test.json中写

   ``` javascript
   {
     "usingComponents": {
       "location": "../../location/location"
     }
   }
   ```

4. 在test.wxml中写

   ``` html
   <location id="addresSelect"></location>
   ```

5. 获取组件中选择的地址数据

   ``` javascript
   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {
    this.address = this.selectComponent("#addresSelect");
   },
   submit:function(){
     wx.showModal({
       title: '获取组件数据',
       content: this.address.data.locationResult,
       showCancel: false
     })
   },  
   ```

   pcas-location-wx-min.js是压缩之后的全国数据

   pcas-location-wx.js是未压缩的全国数据

   **全国省市区数据来源于：https://github.com/modood/Administrative-divisions-of-China**

#### 参与贡献