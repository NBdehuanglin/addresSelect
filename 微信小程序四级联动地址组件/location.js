var Api = require("./pcas-location-wx-min.js");

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    locationResult: "省市区县四级选择器", 
    maskVisual: "hidden",  //模态框是否隐藏
    current: 0,            //swiper的current
    province: [],          //省份数组
    city: [],              //地级市数组
    area: [],              //县级市数组
    street: [],            //街道数组

    provinceName:"请选择", //tab省份名
    cityName:"",          //地级市名 
    areaName:"",          //县级市名
    streetName:""         //街道名

  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeCurrent:function(e){
      this.setData({
        current: e.currentTarget.id
      });
    },
    //显示模态框
    modalShow:function(){
      var animation = wx.createAnimation({
        duration: 500,
        timingFunction: 'ease-in-out',
      });
      this.animation = animation;
      animation.translateY(-350).step();
      this.setData({
        animationData: this.animation.export(),
        maskVisual: 'show'
      });
    },
    //隐藏模态框
    modalHide:function(){
      this.animation.translateY(350).step();
      this.setData({
        animationData: this.animation.export(),
        maskVisual: 'hidden',
        provinceName: "请选择",
        cityName: "",         
        areaName: "",
        streetName: "",
        current: 0,
        city: [],
        area: [],
        street: [],
        provinceSelected:""
      });
    },
    currentChanged:function(e){
      // swiper滚动使得current值被动变化，用于高亮标记
      var current = e.detail.current;
      this.setData({
        current: current
      });
    },
    //省份点击事件
    provinceSelected:function(e){
      //标识当前点击省份，记录其名称与主键id都依赖它
      var name = e.currentTarget.dataset.name;
      var cityArr = [];         //所选中省份下面的地级市数据
      //根据nama选择下面的children数组
      Api.locations.forEach(function(e,i,arr){
        if(e.name == name){
          cityArr = e.children;
        }
      });
      console.log("地级市",cityArr);
      this.setData({
        provinceSelected: name,
        provinceName: name,
        cityName: "请选择地级市",
        areaName: "请选择县级市",
        streetName: "请选择街道",
        city: cityArr,    //保存选中省份下面的所有地级市数据
        current: 1,
        area: [],             
        street: []
      });


    },
    //地级市点击事件
    citySelected:function(e){
      var that = this;
      //标识当前点击地级市，记录其名称与主键id都依赖它
      var name = e.currentTarget.dataset.name;
      var areaArr = [];
      //根据选中的那么选择下面的children数组
      that.data.city.forEach(function(e,i,arr){
        if(e.name == name){
          areaArr = e.children;
        }
      });
      console.log("县级市",areaArr);
      that.setData({
        citySelected: name,
        cityName: name,
        areaName: "请选择县级市",
        area: areaArr,
        current: 2
      })
    },
    //县级市点击事件
    areaSelected:function(e){
      var that = this;
      //标识当前点击地级市，记录其名称与主键id都依赖它
      var name = e.currentTarget.dataset.name;
      var streetArr = [];
      //根据选中的那么选择下面的children数组
      that.data.area.forEach(function (e, i, arr) {
        if (e.name == name) {
          streetArr = e.children;
        }
      });
      console.log("街道", streetArr);
      that.setData({
        areaSelected: name,
        areaName: name,
        streetName: "请选择街道",
        street: streetArr,
        current: 3
      })
    },
    //街道点击事件
    streetSelected:function(e){
      var that = this;
      //标识当前点击地级市，记录其名称与主键id都依赖它
      var name = e.currentTarget.dataset.name;
      that.setData({
        streetSelected: name,
        streetName: name,
      })
    },

    //确认按钮
    confirm:function(){
      var that = this;
      var province,city,area,street,result;
      province = (that.data.provinceName == "请选择") ? "" : that.data.provinceName;
      city = (that.data.cityName == "请选择地级市") ? "" : ("-" + that.data.cityName);
      area = (that.data.areaName == "请选择县级市") ? "" : ("-" + that.data.areaName);
      street = (that.data.streetName == "请选择街道") ? "" : ("-" + that.data.streetName);
      result = province + city + area + street;
      //隐藏模态框
      this.animation.translateY(350).step();
      this.setData({
        animationData: this.animation.export(),
        maskVisual: 'hidden',
        locationResult: result,
        provinceName: "请选择",
        cityName: "",
        areaName: "",
        streetName: "",
        current: 0,
        city: [],
        area: [],
        street: [],
        provinceSelected: ""
      });
    }

  },

  /**
   * 组件生命周期函数，在组件实例进入页面节点树时执行，
   * 注意此时不能调用 setData
   */
  created: function(){

  },

  /**
   * 组件生命周期函数，在组件实例进入页面节点树时执行
   */
  attached: function (){

  },

  /**
   * 组件生命周期函数，在组件布局完成后执行，
   * 此时可以获取节点信息（使用 SelectorQuery ）
   */
  ready: function (){
    
    //加载省份
    let provinceArr = [];
    /*wx.request({
      url: "https://www.xtpgy.com/interface/get_location",
      header: { 'Content-Type': 'application/json' },
      success(res) {
        console.log(res);
      },
      fail(e) {
        console.error(e);
      }
    })*/

    //清除多余的属性，缩短数据长度
    Api.locations.forEach(function(e,i,arr){
      provinceArr[i] = {
        code: e.code,
        name: e.name
      }
    });
    this.setData({
      province: provinceArr
    })
    



  },

  /**
   * 组件生命周期函数，在组件实例被移动到节点树另一个位置时执行
   */
  moved: function (){

  },

  /** 
   * 组件生命周期函数，在组件实例被从页面节点树移除时执行
   */
  detached: function (){

  }


})
